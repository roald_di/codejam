﻿namespace MapReduce.Tests
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using CodeJam.MapReduce;
    using Shouldly;
    using Xunit;
    using Xunit.Abstractions;

    public class Facts
    {
        readonly ITestOutputHelper output;

        public static IEnumerable<object[]> solves_all_problems_input
            => Inputs.TestCases.Select(testCase => new[] { testCase });

        public static IEnumerable<object[]> verify_solutions_input
            => new []
            {
                new object[]{ "D-small-practice.in", Solutions.Small },
                new object[]{ "D-large-practice.in", Solutions.Large }
            };

        public Facts(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Theory]
        [MemberData(nameof(solves_all_problems_input))]
        public void solves_all_problems(TestCase testCase)
        {
            output.WriteLine(message: "Input");
            output.WriteLine(testCase.Input);

            var problem = Problem.Parse(new StringReader(testCase.Input));
            var solved = Solver.Solve(problem);

            output.WriteLine(message: "Solution");
            output.WriteLine(problem.Map.Print());

            solved.ShouldBe(testCase.HasSolution);

            if (testCase.HasSolution)
            {
                var shortestPath = problem.Map.ShortestPath(problem.StartLocation, problem.FinishLocation);
                shortestPath.ShouldBe(problem.TargetDistance);

                VerifyIntegrity(problem.Map).ShouldBe(expected: true);
            }
        }

        [Theory]
        [MemberData(nameof(verify_solutions_input))]
        public void verify_solutions(string inputFile, IReadOnlyList<bool> solutions)
        {
            var failed = false;
            using (var reader = File.OpenText(inputFile))
            {
                var problems = Problem.ParseMany(reader)
                    .Zip(solutions, (problem, solution) => (problem, solution));

                var index = 1;
                foreach (var (problem, solution) in problems)
                {
                    if (Solver.Solve(problem) != solution)
                    {
                        output.WriteLine($"{index} - Failed path existence: expected {solution} but was {!solution}.");
                        output.WriteLine(problem.Map.Print());
                        failed = true;
                    }

                    if (solution)
                    {
                        var shortestPath = problem.Map.ShortestPath(problem.StartLocation, problem.FinishLocation);
                        if (shortestPath != problem.TargetDistance)
                        {
                            output.WriteLine($"{index} - Failed path length: expected {problem.TargetDistance} but was {shortestPath}.");
                            output.WriteLine(problem.Map.Print());
                            failed = true;
                        }

                        if (!VerifyIntegrity(problem.Map))
                        {
                            output.WriteLine($"{index} - Failed integrity.");
                            output.WriteLine(problem.Map.Print());
                            failed = true;
                        }
                    }

                    index++;
                }
            }

            failed.ShouldBe(expected: false);
        }

        bool VerifyIntegrity(Map<Square> map)
        {
            bool RegionIsValid(MapLocation[] region)
            {
                var walls = region
                    .Where(location => map[location].Type == SquareType.Wall)
                    .ToArray();

                if (walls.Length != 2)
                {
                    return true;
                }

                return walls[0].Y == walls[1].Y || walls[0].X == walls[1].X;
            }

            for (var y = 0; y < map.Height - 1; y++)
            {
                for (var x = 0; x < map.Width - 1; x++)
                {
                    var region = new[]
                    {
                        new MapLocation(x, y),
                        new MapLocation(x + 1, y),
                        new MapLocation(x, y + 1),
                        new MapLocation(x + 1, y + 1)
                    };

                    if (!RegionIsValid(region))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}