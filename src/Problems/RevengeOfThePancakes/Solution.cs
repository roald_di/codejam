﻿namespace CodeJam.RevengeOfThePancakes
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Common;
    using Common.IO;
    using Common.Linq;

    public class Solution : ISolution
    {
        public void Write(StreamReader reader, SolutionWriter writer)
        {
            var input = reader.ReadAllLines()
                .Skip(count: 1)
                .Select(Parse);

            foreach (var pancakes in input)
            {
                var solution = Solve(pancakes);

                writer.WriteTestCase(solution.ToString());
            }
        }

        static IReadOnlyList<bool> Parse(string input)
            => input.Select(character => character == '+').ToList();

        static int Solve(IReadOnlyList<bool> pancakes)
        {
            if (pancakes.Count == 0)
            {
                return 0;
            }

            var last = pancakes[pancakes.Count - 1];
            var flips = pancakes
                .DistinctUntilChanged()
                .Count();

            return last ? flips - 1 : flips;
        }
    }
}