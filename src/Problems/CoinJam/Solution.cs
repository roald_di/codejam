﻿namespace CodeJam.CoinJam
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Common;
    using Common.IO;

    public class Solution : ISolution
    {
        public void Write(StreamReader reader, SolutionWriter writer)
        {
            var input = reader.ReadAllLines()
                .Skip(count: 1)
                .Select(Parse);

            foreach (var (n, j) in input)
            {
                writer.WriteTestCase();

                foreach (var coin in Solve(n).Take(j))
                {
                    writer.WriteLine(coin);
                }
            }
        }

        (int n, int j) Parse(string input)
        {
            var split = input.Split(' ');

            return (int.Parse(split[0]), int.Parse(split[1]));
        }

        public static IEnumerable<string> Solve(int n)
        {
            foreach (var combination in Combinations(n))
            {
                var factors = FindFactorsForAllBases(n, combination).ToList();
                if (factors.Count == 9)
                {
                    yield return combination.ToBinaryString(n) + " " + string.Join(" ", factors);
                }
            }
        }

        public static IEnumerable<ulong> FindFactorsForAllBases(int n, ulong combination)
        {
            for (var baseN = 2u; baseN <= 10; baseN++)
            {
                var candidate = ConvertToBase(n, baseN, combination);
                var factor = TryFindFactor(candidate);
                if (factor > 0)
                {
                    yield return factor;
                }
            }
        }

        public static ulong TryFindFactor(ulong candidate)
        {
            var limit = Math.Sqrt(candidate);
            for (var factor = 2u; factor <= limit; factor++)
            {
                if (candidate % factor == 0)
                {
                    return factor;
                }
            }
            return 0;
        }

        public static IEnumerable<ulong> Combinations(int n)
        {
            var variableSlots = n - 2;
            var max = (1u << variableSlots) - 1;
            var upper = 1u << (n - 1);

            for (var i = 0u; i <= max; i++)
            {
                yield return ((i << 1) + 1) | upper;
            }
        }

        public static ulong ConvertToBase(int n, ulong baseN, ulong number)
        {
            checked
            {
                if (baseN == 2)
                {
                    return number;
                }

                var mult = 1ul;
                var result = 0ul;
                for (var i = 0; i < n; i++)
                {
                    var pos = 1u << i;
                    if ((number & pos) != 0)
                    {
                        result += mult;
                    }

                    mult *= baseN;
                }

                return result;
            }
        }
    }
}