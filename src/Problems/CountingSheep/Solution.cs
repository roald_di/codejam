﻿namespace CodeJam.CountingSheep
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Common;
    using Common.IO;

    public class Solution : ISolution
    {
        public void Write(StreamReader reader, SolutionWriter writer)
        {
            var input = reader.ReadAllLines()
                .Skip(count: 1)
                .Select(int.Parse);

            foreach (var n in input)
            {
                var solution = Solve(n)?.ToString() ?? "INSOMNIA";

                writer.WriteTestCase(solution);
            }
        }

        static int? Solve(int n)
        {
            if (n == 0)
            {
                return null;
            }

            var digits = new HashSet<int>();

            for (var multiplier = 1;; multiplier++)
            {
                var current = n * multiplier;

                digits.AddRange(current.GetDigits());

                if (digits.Count == 10)
                {
                    return current;
                }
            }
        }
    }
}