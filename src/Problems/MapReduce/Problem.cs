﻿namespace CodeJam.MapReduce
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class Problem
    {
        public Map<Square> Map { get; }
        public int TargetDistance { get; }
        public MapLocation StartLocation { get; }
        public MapLocation FinishLocation { get; }

        Problem(Map<Square> map, int targetDistance, MapLocation startLocation, MapLocation finishLocation)
        {
            Map = map;
            TargetDistance = targetDistance;
            StartLocation = startLocation;
            FinishLocation = finishLocation;
        }
        
        public void Deconstruct(out Map<Square> map, out int targetDistance, out MapLocation startLocation, out MapLocation finishLocation)
        {
            map = Map;
            targetDistance = TargetDistance;
            startLocation = StartLocation;
            finishLocation = FinishLocation;
        }

        public static IEnumerable<Problem> ParseMany(TextReader reader)
        {
            var line = reader.ReadLine();
            if (line == null)
            {
                throw new InvalidOperationException();
            }
            
            var problemCount = int.Parse(line);
            for (var index = 0; index < problemCount; index++)
            {
                yield return Parse(reader);
            }
        }

        public static Problem Parse(TextReader reader)
        {
            var line = reader.ReadLine();
            if (line == null)
            {
                throw new InvalidOperationException();
            }
            
            var header = line.Split(' ');
            var height = int.Parse(header[0]);
            var width = int.Parse(header[1]);
            var distance = int.Parse(header[2]);

            var map = new Map<Square>(width, height);

            var start = default(MapLocation);
            var finish = default(MapLocation);

            for (var y = 0; y < height; y++)
            {
                var mapRow = reader.ReadLine();
                if (mapRow == null)
                {
                    throw new InvalidOperationException();
                }
                
                for (var x = 0; x < map.Width; x++)
                {
                    var squareType = x== 0 || x == map.Width -1 || y == 0 || y == map.Height -1 
                        ? SquareType.Border 
                        : SquareTypes.Parse(mapRow[x]);

                    map[x, y] = new Square(squareType);

                    switch (squareType)
                    {
                        case SquareType.Start:
                            start = new MapLocation(x, y);
                            break;
                        case SquareType.Finish:
                            finish = new MapLocation(x, y);
                            break;
                    }
                }
            }

            return new Problem(map, distance, start, finish);
        }
    }
}