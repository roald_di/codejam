﻿namespace CodeJam.MapReduce
{
    using System;
    using System.Collections.Generic;

    public static class Traversals
    {
        public static int ShortestPath(this Map<Square> map, MapLocation startLocation, MapLocation finishLocation)
        {
            foreach (var (location, distance) in map.Traverse(startLocation, map.GetEmptyNeighbours))
            {
                if (location == finishLocation)
                {
                    return distance;
                }
            }

            return -1;
        }

        public static IEnumerable<(MapLocation, int)> Traverse(
            this Map<Square> map,
            MapLocation startLocation,
            Func<MapLocation, MapLocation[]> edges)
        {
            var state = new Map<(bool visited, int distance)>(map.Width, map.Height);

            var queue = new Queue<MapLocation>();
            queue.Enqueue(startLocation);

            state[startLocation] = (visited: true, distance: 0);

            while (queue.Count != 0)
            {
                var current = queue.Dequeue();
                var (_, distance) = state[current];

                yield return (current, distance);

                foreach (var neighbour in edges(current))
                {
                    var neighbourState = state[neighbour];

                    if (!neighbourState.visited)
                    {
                        state[neighbour] = (visited: true, distance: distance + 1);
                        queue.Enqueue(neighbour);
                    }
                }
            }
        }
    }
}