﻿namespace CodeJam.MapReduce
{
    using System;
    using System.Linq;

    public static class Solver
    {
        public static bool Solve(Problem problem)
        {
            var (map, targetDistance, startLocation, finishLocation) = problem;

            ref var start = ref map[startLocation];
            start.DistanceFromStart = 0;

            foreach (var (location, distance) in map.Traverse(finishLocation, map.GetEmptyNeighbours))
            {
                map[location].DistanceFromFinish = distance;
            }

            var shortestPath = start.DistanceFromFinish;
            if (shortestPath < targetDistance)
            {
                return false;
            }

            if ((int)shortestPath == targetDistance)
            {
                return true;
            }

            return RemoveWalls(map, startLocation, targetDistance);
        }

        static bool RemoveWalls(Map<Square> map, MapLocation startLocation, int targetDistance)
        {
            foreach (var (location, _) in map.Traverse(startLocation, map.GetEmptyOrRemovableNeighbours).Skip(1))
            {
                var (fromStart, fromFinish) = map.GetShortestPaths(location);

                ref var current = ref map[location];
                current.DistanceFromStart = fromStart;
                current.DistanceFromFinish = fromFinish;

                var shortestPath = fromStart + fromFinish;

                if (current.Type == SquareType.Wall && shortestPath >= targetDistance)
                {
                    current.Type = SquareType.Empty;
                }

                if ((int)shortestPath == targetDistance)
                {
                    return true;
                }
            }

            return false;
        }

        static (float fromStart, float fromFinish) GetShortestPaths(this Map<Square> map, MapLocation location)
        {
            var fromStart = float.PositiveInfinity;
            var fromFinish = float.PositiveInfinity;

            foreach (var neighbour in map.GetEmptyNeighbours(location))
            {
                var current = map[neighbour];

                fromStart = Math.Min(fromStart, current.DistanceFromStart);
                fromFinish = Math.Min(fromFinish, current.DistanceFromFinish);
            }

            return (fromStart + 1, fromFinish + 1);
        }
    }
}