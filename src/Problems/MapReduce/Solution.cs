﻿namespace CodeJam.MapReduce
{
    using System.IO;
    using Common;
    using Common.IO;

    public class Solution : ISolution
    {
        public void Write(StreamReader reader, SolutionWriter writer)
        {
            var problems = Problem.ParseMany(reader);

            foreach (var problem in problems)
            {
                var solved = Solver.Solve(problem);

                writer.WriteTestCase(solved ? "POSSIBLE" : "IMPOSSIBLE");
                if (solved)
                {
                    writer.Write(problem.Map.Print());
                }
            }
        }
    }
}