namespace CodeJam.MapReduce
{
    using System;

    public class Map<T>
    {
        readonly T[] items;

        public int Height { get; }
        public int Width { get; }

        public ref T this[MapLocation location]
            => ref items[location.Y * Width + location.X];

        public ref T this[int x, int y]
            => ref items[y * Width + x];

        public Map(int width, int height)
        {
            Height = height;
            Width = width;
            items = new T[width * height];
        }
    }

    public struct MapLocation
    {
        public int X { get; }
        public int Y { get; }

        public MapLocation(int x, int y)
        {
            X = x;
            Y = y;
        }

        public bool Equals(MapLocation other)
            => X == other.X && Y == other.Y;

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(objA: null, obj))
            {
                return false;
            }

            return obj is MapLocation location && Equals(location);
        }

        public override int GetHashCode()
            => HashCode.Combine(X, Y);

        public static bool operator ==(MapLocation left, MapLocation right)
            => left.Equals(right);

        public static bool operator !=(MapLocation left, MapLocation right)
            => !left.Equals(right);

        public static MapLocation operator +(MapLocation left, MapLocation right)
            => new MapLocation(left.X + right.X, left.Y + right.Y);
    }
}