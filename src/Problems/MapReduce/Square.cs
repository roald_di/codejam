namespace CodeJam.MapReduce
{
    using System;

    [Flags]
    public enum SquareType
    {
        Empty = 1,
        Start = 2 | Empty,
        Finish = 4 | Empty,
        Wall = 8,
        Border = 16
    }

    public struct Square
    {
        public SquareType Type { get; set; }

        public float DistanceFromFinish { get; set; }
        public float DistanceFromStart { get; set; }

        public Square(SquareType type)
        {
            Type = type;
            DistanceFromFinish = float.PositiveInfinity;
            DistanceFromStart = float.PositiveInfinity;
        }
    }

    public static class SquareTypes
    {
        public static SquareType Parse(char value)
        {
            switch (value)
            {
                case 'S':
                    return SquareType.Start;
                case 'F':
                    return SquareType.Finish;
                case '#':
                    return SquareType.Wall;
                default:
                    return SquareType.Empty;
            }
        }

        public static char Print(this SquareType value)
        {
            switch (value)
            {
                case SquareType.Start:
                    return 'S';
                case SquareType.Finish:
                    return 'F';
                case SquareType.Wall:
                case SquareType.Border:
                    return '#';
                default:
                    return '.';
            }
        }
    }
}