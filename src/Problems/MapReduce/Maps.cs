namespace CodeJam.MapReduce
{
    using System;
    using System.Text;

    public static class Maps
    {
        public static MapLocation[] GetNeighbours<T>(this Map<T> map, MapLocation location)
        {
            return new[]
            {
                new MapLocation(location.X, location.Y - 1),
                new MapLocation(location.X + 1, location.Y),
                new MapLocation(location.X, location.Y + 1),
                new MapLocation(location.X - 1, location.Y)
            };
        }

        public static MapLocation[] GetEmptyNeighbours(this Map<Square> map, MapLocation location)
        {
            var neighbours = map.GetNeighbours(location);
            var write = 0;
            for (var read = 0; read < neighbours.Length; read++)
            {
                var neighbour = neighbours[read];
                if (map[neighbour].Type.HasFlag(SquareType.Empty))
                {
                    neighbours[write] = neighbour;
                    write++;
                }
            }

            Array.Resize(ref neighbours, write);
            return neighbours;
        }

        public static MapLocation[] GetEmptyOrRemovableNeighbours(this Map<Square> map, MapLocation location)
        {
            if (map[location].Type == SquareType.Wall)
            {
                return Array.Empty<MapLocation>();
            }

            var neighbours = map.GetNeighbours(location);
            var write = 0;
            for (var read = 0; read < neighbours.Length; read++)
            {
                var neighbour = neighbours[read];
                if (map[neighbour].Type != SquareType.Border && (map[neighbour].Type != SquareType.Wall || map.WallIsRemovable(neighbour)))
                {
                    neighbours[write] = neighbour;
                    write++;
                }
            }

            Array.Resize(ref neighbours, write);
            return neighbours;
        }

        static bool WallIsRemovable(this Map<Square> map, MapLocation location)
        {
            var x = location.X;
            var y = location.Y;

            var leftIsWall = map[x - 1, y].Type == SquareType.Wall;
            var topIsWall = map[x, y - 1].Type == SquareType.Wall;

            if (leftIsWall && map[x - 1, y - 1].Type.HasFlag(SquareType.Empty) && topIsWall)
            {
                return false;
            }

            var rightIsWall = map[x + 1, y].Type == SquareType.Wall;
            if (topIsWall && map[x + 1, y - 1].Type.HasFlag(SquareType.Empty) && rightIsWall)
            {
                return false;
            }

            var bottomIsWall = map[x, y + 1].Type == SquareType.Wall;
            if (rightIsWall && map[x + 1, y + 1].Type.HasFlag(SquareType.Empty) && bottomIsWall)
            {
                return false;
            }

            if (bottomIsWall && map[x - 1, y + 1].Type.HasFlag(SquareType.Empty) && leftIsWall)
            {
                return false;
            }

            return true;
        }

        public static string Print(this Map<Square> map)
        {
            var result = new StringBuilder(map.Width * map.Height);

            for (var row = 0; row < map.Height; row++)
            {
                for (var column = 0; column < map.Width; column++)
                {
                    result.Append(map[column, row].Type.Print());
                }

                result.AppendLine();
            }

            return result.ToString();
        }

        public static string Print(this Map<Square> map, MapLocation location)
        {
            var result = new StringBuilder(map.Width * map.Height);

            for (var row = 0; row < map.Height; row++)
            {
                for (var column = 0; column < map.Width; column++)
                {
                    if (location == new MapLocation(column, row))
                    {
                        result.Append('X');
                    }
                    else
                    {
                        result.Append(map[column, row].Type.Print());
                    }
                }

                result.AppendLine();
            }

            return result.ToString();
        }
    }
}