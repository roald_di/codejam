﻿namespace CodeJam.Run
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using Common;
    using Common.IO;

    static class Program
    {
        static readonly IReadOnlyDictionary<string, ISolution> solutions = new Dictionary<string, ISolution>
        {
            { "map-reduce", new MapReduce.Solution() },
            { "counting-sheep", new CountingSheep.Solution() },
            { "revenge-of-the-pancakes", new RevengeOfThePancakes.Solution() },
            { "coin-jam", new CoinJam.Solution() }
        };

        static void Main(string[] args)
        {
            var root = new DirectoryInfo(@"..\..\data");

            var problems = DiscoverProblems(root)
                .ToList();

            var watch = new Stopwatch();

            Console.WriteLine("warmup..");
            foreach (var problem in problems)
            {
                var inputFile = problem.File;
                using (var reader = File.OpenText(inputFile))
                {
                    if (solutions.TryGetValue(problem.Id, out var solution))
                    {
                        solution.Write(reader, new SolutionWriter(Stream.Null));
                    }
                }
            }

            foreach (var problem in problems)
            {
                Console.Write($"solving {problem.DisplayName}: ");

                var inputFile = problem.File;
                var outputFile = Path.ChangeExtension(inputFile, ".out");

                File.Delete(outputFile);

                using (var outputStream = File.OpenWrite(outputFile))
                using (var writer = new SolutionWriter(outputStream))
                using (var reader = File.OpenText(inputFile))
                {
                    if (solutions.TryGetValue(problem.Id, out var solution))
                    {
                        watch.Restart();
                        solution.Write(reader, writer);

                        var elapsed = watch.ElapsedMilliseconds;
                        Console.WriteLine($"{elapsed}ms.");
                    }
                    else
                    {
                        Console.WriteLine("No solver found for this problem.");
                    }
                }
            }
        }

        static IEnumerable<Problem> DiscoverProblems(DirectoryInfo root)
        {
            var inputFilePattern = "*.in";
            var problems = root
                .GetFiles(inputFilePattern, SearchOption.AllDirectories)
                .OrderBy(file => file.DirectoryName)
                .ThenByDescending(file => file.Name)
                .Select(Problem.FromFile);

            return problems;
        }
    }
}