namespace CodeJam.Run
{
    using System.IO;

    class Problem
    {
        public string Id { get; }
        public string DisplayName { get; }
        public string File { get; }

        public Problem(string id, string displayName, string file)
        {
            DisplayName = displayName;
            File = file;
            Id = id;
        }

        public static Problem FromFile(FileInfo file)
        {
            var name = file.Directory.Name;
            var type = Path.GetFileNameWithoutExtension(file.Name);

            return new Problem(name, $"'{name}' {type}", file.FullName);
        }
    }
}