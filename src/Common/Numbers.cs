﻿namespace CodeJam.Common
{
    using System.Collections.Generic;

    public static class Numbers
    {
        public static int MostSignificantBit(this ulong number)
        {
            var result = 0;

            while (number > 0)
            {
                result++;
                number >>= 1;
            }

            return result;
        }

        public static IEnumerable<int> GetDigits(this int number, int inBase = 10)
        {
            while (number > 0)
            {
                yield return number % inBase;
                number /= inBase;
            }
        }

        public static string ToBinaryString(this ulong number, int? totalLength = null)
        {
            var n = totalLength ?? number.MostSignificantBit();
            var chars = new char[n];

            for (var i = 0; i < n; i++)
            {
                var pos = 1u << i;

                chars[n - i - 1] = (number & pos) != 0 ? '1' : '0';
            }

            return new string(chars);
        }
    }
}