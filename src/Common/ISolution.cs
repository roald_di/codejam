﻿namespace CodeJam.Common
{
    using System.IO;
    using IO;

    public interface ISolution
    {
        void Write(StreamReader reader, SolutionWriter writer);
    }
}