﻿namespace CodeJam.Common
{
    using System.Collections.Generic;
    using System.Linq;

    public static class Sets
    {
        public static bool AddRange<T>(this ISet<T> set, IEnumerable<T> items)
            => items.Aggregate(seed: true, func: (added, item) => set.Add(item) && added);
    }
}