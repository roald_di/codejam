﻿namespace CodeJam.Common.IO
{
    using System.Collections.Generic;
    using System.IO;

    public static class StreamReaders
    {
        public static IEnumerable<string> ReadAllLines(this StreamReader reader)
        {
            while (!reader.EndOfStream)
            {
                yield return reader.ReadLine();
            }
        }
    }
}