﻿namespace CodeJam.Common.IO
{
    using System.IO;
    using System.Text;

    public class SolutionWriter : StreamWriter
    {
        int currentTestCase = 1;

        public SolutionWriter(Stream stream)
            : base(stream)
        {
        }

        public SolutionWriter(Stream stream, Encoding encoding)
            : base(stream, encoding)
        {
        }

        public SolutionWriter(Stream stream, Encoding encoding, int bufferSize)
            : base(stream, encoding, bufferSize)
        {
        }

        public SolutionWriter(Stream stream, Encoding encoding, int bufferSize, bool leaveOpen)
            : base(stream, encoding, bufferSize, leaveOpen)
        {
        }

        public void WriteTestCase(string text = null)
        {
            Write($"Case #{currentTestCase}:");

            if (text != null)
            {
                Write($" {text}");
            }

            WriteLine();
            currentTestCase++;
        }
    }
}