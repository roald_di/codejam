﻿namespace CodeJam.Common.Linq
{
    using System.Collections.Generic;

    public static class Enumerables
    {
        public static IEnumerable<T> DistinctUntilChanged<T>(this IEnumerable<T> source) 
            => DistinctUntilChanged(source, EqualityComparer<T>.Default);

        public static IEnumerable<T> DistinctUntilChanged<T>(this IEnumerable<T> source, IEqualityComparer<T> comparer)
        {
            using (var enumerator = source.GetEnumerator())
            {
                if (!enumerator.MoveNext())
                {
                    yield break;
                }

                var last = enumerator.Current;

                yield return last;

                while (enumerator.MoveNext())
                {
                    var current = enumerator.Current;
                    if (!comparer.Equals(last, current))
                    {
                        last = current;
                        yield return current;
                    }
                }
            }
        }
    }
}